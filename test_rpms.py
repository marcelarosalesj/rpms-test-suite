import pytest

from utils import komander

class TestOpenSuseRpms:

    REPO_URL=("https://download.opensuse.org/repositories/"
              "Cloud:/StarlingX:/2.0/openSUSE_Leap_15.1/")

    packages = ["fm-common", "fm-mgr", "fm-api", "fm-rest-api",
                "snmp-ext", "snmp-audittrail"]

    def setup_class(self):
        cmd = "zypper addrepo {} starlingx".format(self.REPO_URL)
        res = komander.run(cmd)
        assert res.retcode == 0, "Cannot add starlingx repo: {}".format(res)

        res = komander.run("zypper --gpg-auto-import-keys refresh")
        assert res.retcode == 0, "Cannot refresh repo: {}".format(res)

    def teardown_class(self):
        cmd = "zypper removerepo starlingx"
        res = komander.run(cmd)
        assert res.retcode == 0, "Cannot remove starlingx repo: {}".format(res)

    @pytest.mark.parametrize("input_package", packages)
    def test_install(self, input_package):
        cmd = "zypper install -y {}".format(input_package)
        res = komander.run(cmd)
        assert res.retcode == 0, "Cannot install package fm-common: {}".format(res)

        cmd = "zypper remove -y {}".format(input_package)
        res = komander.run(cmd)
        assert res.retcode == 0, "Cannot remove package fm-common: {}".format(res)
