# rpms test suite

A simple test suite to verify the installation of rpms. For now, only opensuse
test are supported.

## Running the tests

The simplest way to run these test is using a container.

```
docker pull opensuse/leap:15.1
```

Then run the container.

```
$ cd <path to this repo>
$ docker run -ti -v $(pwd)/work -w /work opensuse/leap:15.1 bash
$ zypper in -y python2-pip
$ pip install -r requirements.txt
$ pytest -s -v --junit-xml=results.txt
```
